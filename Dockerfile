FROM node:13-alpine

ENV NODE_ENV production

WORKDIR /nuxt-app
ADD . /nuxt-app/

RUN npm i
RUN cp .env.example .env

RUN npm run test

RUN npm run build

CMD ["npm", "run", "dev"]
